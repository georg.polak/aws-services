var AWS = require('aws-sdk');
// AWS.config.loadFromPath('./.aws_credential.json');
const topics = {
  'badges': 'arn:aws:sns:eu-central-1:762110983066:S4Me-badges.fifo'
}

function createTopic(topicName) {
// Create promise and SNS service object
var createTopicPromise = new AWS.SNS({}).createTopic({Name: topicName}).promise();

// Handle promise's fulfilled/rejected states
createTopicPromise.then(
  function(data) {
    console.log("Topic ARN is " + data.TopicArn);
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });
}

function listTopics() {
  var listTopicsPromise = new AWS.SNS({}).listTopics({}).promise();

  // Handle promise's fulfilled/rejected states
  listTopicsPromise.then(
    function(data) {
      // console.log(data.Topics);
      if(data.Topics.length > 0) {
        for(let i=0;i<data.Topics.length;i++) {
          console.log(data.Topics[i]);
          getTopicAttributes(data.Topics[i].TopicArn)
        }
      }
    }).catch(
      function(err) {
      console.error(err, err.stack);
    });
}
function getTopicAttributes(topicArn) {
  var getTopicAttribsPromise = new AWS.SNS({}).getTopicAttributes({TopicArn: topicArn}).promise();

  // Handle promise's fulfilled/rejected states
  getTopicAttribsPromise.then(
    function(data) {
      console.log(data);
    }).catch(
      function(err) {
      console.error(err, err.stack);
    });
}


function deleteTopic(topicArn) {
  // Create promise and SNS service object
  var deleteTopicPromise = new AWS.SNS({apiVersion: '2010-03-31'}).deleteTopic({TopicArn: topicArn}).promise();

  // Handle promise's fulfilled/rejected states
  deleteTopicPromise.then(
    function(data) {
      console.log("Topic Deleted");
    }).catch(
      function(err) {
      console.error(err, err.stack);
    });
}

function publish(topicArn) {
  // Create publish parameters
  var params = {
    Message: 'MESSAGE_TEXT', /* required */
    TopicArn: topicArn
  };

  // Create promise and SNS service object
  var publishTextPromise = new AWS.SNS({}).publish(params).promise();

  // Handle promise's fulfilled/rejected states
  publishTextPromise.then(
    function(data) {
      console.log(`Message ${params.Message} sent to the topic ${params.TopicArn}`);
      console.log("MessageID is " + data.MessageId);
    }).catch(
      function(err) {
      console.error(err, err.stack);
    });
}


function listSubscriptionsByTopic(topicArn) {
  const params = {
    TopicArn : topicArn
  }

  // Create promise and SNS service object
  var subslistPromise = new AWS.SNS({apiVersion: '2010-03-31'}).listSubscriptionsByTopic(params).promise();

  // Handle promise's fulfilled/rejected states
    subslistPromise.then(
      function(data) {
        console.log(data);
      }).catch(
      function(err) {
        console.error(err, err.stack);
      }
    );
  // createTopic('TST');
  // listTopics();
  // deleteTopic('arn:aws:sns:eu-central-1:762110983066:TST');

  // publish(topics['badges']);
  listSubscriptionsByTopic(topics['badges']);
}

