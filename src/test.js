const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

const yargs = require('yargs');
const SQS = require('./SQS');

const argv = yargs.options({
    cmd: {
      alias: 'c',
      type: 'string'
    }
}).argv;

require('./SQS');

async function test() {
    try {
        if(argv.cmd) {
            switch(argv.cmd) {
                case 'list':
                    // console.log('LIST');
                    // var x = await SQS.createQueue('sports4me-badgeService');
                    // let url = await SQS.getQueueUrl('sports4me-badgeService');
                    let list = await SQS.listQueues();
                    console.log(list);
                break;
                case 'send':
                    console.log('SEND');
                    SQS.sendMessage('sports4me-badgeService', {body:{badgeAwardId:"600413b9ef22d1dfd7a12429"}});
                break;
                case 'receive':
                    console.log('RECEIVE');
                    SQS.receiveMessage('sports4me-badgeService', true);
                break;

                case 'pull':
                    SQS.startPulling('sports4me-badgeService', (a) => {console.log('cbF', a);});
                break;
                }
            }
    } catch (e) {
    }
}

test();
